############################################
# Setup Server
############################################

set :user, "root"
set :host, "192.241.130.226"
server "#{host}", :app
set :deploy_to, "/var/www/sites/staging.pbfuniversity.com"

############################################
# Setup Git
############################################

set :branch, "master"