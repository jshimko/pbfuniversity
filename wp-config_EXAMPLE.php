<?php
    define('DB_NAME', '');
    define('DB_USER', '');
    define('DB_PASSWORD', '');
    define('DB_HOST', '');
    define('DB_CHARSET', 'utf8');
    define('DB_COLLATE', ''); 
	define('WPLANG', '');

	$table_prefix  = 'wp_';

	define('AUTH_KEY',         '');
	define('SECURE_AUTH_KEY',  '');
	define('LOGGED_IN_KEY',    '');
	define('NONCE_KEY',        '');
	define('AUTH_SALT',        '');
	define('SECURE_AUTH_SALT', '');
	define('LOGGED_IN_SALT',   '');
	define('NONCE_SALT',       '');

	define('WP_HOME','/');
	define('WP_SITEURL','/w');

	define('WP_CONTENT_URL', 'http://localhost/content');
	define( 'WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/content' );

	if ( !defined('ABSPATH') )
	        define('ABSPATH', dirname(__FILE__) . '/');

	require_once(ABSPATH . 'wp-settings.php');
